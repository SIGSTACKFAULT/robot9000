FROM python:3.10.11
RUN apt update
RUN apt install -y ffmpeg
ADD main.py main.py
ADD requirements.txt requirements.txt
RUN pip install -r requirements.txt
RUN cat /etc/hosts
ADD robot9000/ robot9000/
ENTRYPOINT python main.py --db r9k