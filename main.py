#!/usr/bin/env python3

"""ROBOT9000 entry point."""

import asyncio
import logging
import logging.handlers
import os

import click
from discord.ext import commands
import motor.motor_asyncio

import robot9000


logger = logging.getLogger(__name__)


@click.command()
@click.option(
    "--db",
    required=True,
    type=str,
    metavar="DATABASE",
    help="Database to use. Prod is traditionally 'r9k'",
)
@click.option(
    "-h",
    "--host",
    type=str,
    metavar="HOST",
    default=None,
    help="Host to connect to. Defaults to `localhost`.",
)
@click.option(
    "-p",
    "--port",
    type=str,
    metavar="PORT",
    default=None,
    help="Port to connect to. Defautls to `27017`.",
)
@click.option("--prefix", default="r9;", metavar="PREFIX", help="command prefix")
@click.option(
    "--ping-prefix/--no-ping-prefix",
    default=True,
    help="Enable or disable pinging the bot instead of using it's prefix?",
)
@click.option(
    "--jokes/--no-jokes",
    default=True,
)
def main(**kwargs):
    # loop = asyncio.get_running_loop()
    # asyncio.create_task(amain(**kwargs))
    # loop.run_forever()
    asyncio.run(amain(**kwargs))

async def amain(
    db: str,
    host: str,
    port: int,
    prefix: str,
    ping_prefix: bool,
    jokes: bool,
):
    logging.basicConfig(
        level=logging.INFO, format="[%(levelname)7s] %(name)s: %(message)s"
    )

    # cut down the spam on stderr just a bit
    logging.getLogger("discord.gateway").setLevel(logging.WARN)
    logging.getLogger("discord.client").setLevel(logging.WARN)

    motor_client = motor.motor_asyncio.AsyncIOMotorClient(
        host=host,
        port=port,
    )

    if ping_prefix:
        command_prefix = commands.when_mentioned_or(prefix)
    else:
        command_prefix = prefix
    logger.debug(f"command_prefix: {command_prefix!r}")

    database = motor_client[db]

    bot = robot9000.ROBOT9000(db=database, command_prefix=command_prefix)
    await bot.load_extension("robot9000.ext.background")
    await bot.load_extension("robot9000.ext.commands")
    await bot.load_extension("robot9000.ext.listener")

    if jokes:
        await bot.load_extension("robot9000.ext.jokes")

    bot.owner_ids = {  # this has to be a set for some reason
        247841704386756619,  # SIGSTKFLT
        415004648555151380,  # Hopeless_Platonic
    }

    # token = token_file.read().strip()
    # token_file.close()

    if not "TOKEN" in os.environ:
        logger.error("expected TOKEN environment variable")
        return

    await bot.start(os.environ["TOKEN"].strip())


main()  # pylint: disable=no-value-for-parameter
