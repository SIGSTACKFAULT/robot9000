"""
Experimental chat moderation tool for Discord, based on an xkcd blog post.
"""

__version__ = "1.0.0"

from robot9000.bot import ROBOT9000
