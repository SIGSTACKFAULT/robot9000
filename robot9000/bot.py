"""
ROBOT9000 bot object definition.
"""

import asyncio
import typing
from typing import Union

import discord
from discord.ext import commands
import motor.motor_asyncio as motor

from .helpers import NoSuchChannelException
from .model import ChannelConfig

Channel = discord.TextChannel | discord.VoiceChannel | discord.StageChannel | discord.Thread | discord.abc.PrivateChannel | discord.PartialMessageable

def practical_id(channel: Channel) -> int:
    """parent id if thread, otherwise just the id"""
    if isinstance(channel, discord.Thread):
        return channel.parent_id
    if isinstance(channel, (discord.abc.GuildChannel | discord.PartialMessageable)):
        return channel.id
    raise ValueError(f"unexpected {channel.__class__.__name__}: {channel!r}")

class ROBOT9000(commands.Bot):
    """ROBOT9000 bot object. Handles database stuff."""

    def __init__(
        self,
        command_prefix,
        db: motor.AsyncIOMotorDatabase,
        *,
        error_traceback_buttons: bool = True,
    ):
        intents = discord.Intents.default()
        intents.message_content = True
        super().__init__(
            command_prefix,
            intents=intents,
        )
        self.db = db
        self.error_traceback_buttons: bool = error_traceback_buttons

    @property
    def config(self) -> motor.AsyncIOMotorCollection:
        """Collection which holds channel configurations"""
        return self.db["config"]

    async def get_channel_config(
        self,
        channel: Channel,
        extras: dict = {},
    ) -> ChannelConfig:
        query_id = practical_id(channel)
        result = await self.config.find_one({"_id": query_id, **extras})
        if result is None:
            raise NoSuchChannelException(channel)
        return result

    async def update_channel_config(
        self,
        channel: Channel,
        update: dict,
        upsert: bool = False,
    ):
        query_id = practical_id(channel)
        await self.config.update_one(
            {"_id": query_id},
            update,
            upsert=upsert,
        )

    async def set_channel_config(
        self,
        channel: Channel,
        changes: typing.TypedDict,
        upsert: bool = False,
    ):
        return await self.update_channel_config(
            channel, {"$set": changes}, upsert=upsert
        )

    async def teardown_channel(
        self,
        channel: Channel,
    ):
        """Delete all data for the given channel.
        Doesn't ask any questions."""
        query_id = practical_id(channel)
        await asyncio.gather(
            self.db.config.delete_one({"_id": query_id}),
            self.channel_messages(channel).drop(),
            self.channel_attachments(channel).drop(),
            self.channel_users(channel).drop(),
        )

    def channel_messages(
        self,
        channel: Channel,
    ) -> motor.AsyncIOMotorCollection:
        """Return the collection which contains the message data
        for `channel`."""
        query_id = practical_id(channel)
        return self.db[f"channels.{query_id}.messages"]

    def channel_attachments(
        self,
        channel: Channel,
    ) -> motor.AsyncIOMotorCollection:
        """Return the collection which contains the attachment data
        for `channel`."""
        query_id = practical_id(channel)
        return self.db[f"channels.{query_id}.attachments"]

    def channel_users(
        self,
        channel: Channel,
    ) -> motor.AsyncIOMotorCollection:
        """Return the collection which contains the user data
        for `channel`."""
        query_id = practical_id(channel)
        return self.db[f"channels.{query_id}.users"]

    def punishments(self) -> motor.AsyncIOMotorCollection:
        """Return the collection we store punishments in."""
        return self.db["punishments"]

    async def get_user_data(
        self,
        channel: Channel,
        user: discord.User | discord.Member,
    ):
        """Get the existing data for `user` in `channel`,
        or create a new data for them if none found"""
        collection = self.channel_users(channel)
        existing_data = await collection.find_one({"_id": user.id})
        if existing_data is None:
            existing_data = {"_id": user.id, "streak": 0}
            await collection.insert_one(existing_data)
        return existing_data
