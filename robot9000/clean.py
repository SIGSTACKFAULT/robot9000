import logging
import re
from typing import Iterable
import unicodedata
import functools
import itertools
from string import ascii_uppercase, ascii_letters

import requests

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# don't want to check this in because copyright
CONFUSABLES_URL = \
    "https://www.unicode.org/Public/security/14.0.0/confusables.txt"
CONFUSABLES = {}

for line in requests.get(CONFUSABLES_URL).text.split("\n"):
    if line.strip() == "" or line.startswith("#"):
        continue
    parts = line.split(";")
    if len(parts) != 3:
        continue
    key = chr(int(parts[0], 16))
    value = map(functools.partial(int, base=16), parts[1].split())
    value = "".join(map(chr, value))
    CONFUSABLES[key] = value

# generate confusables from small caps to lowercase
# https://stackoverflow.com/questions/55717223/convert-unicode-small-capitals-to-their-ascii-equivalents
autogenenerated_confusables = 0
for letter in ascii_uppercase:
    try:
        smallcap = unicodedata.lookup(f"LATIN LETTER SMALL CAPITAL {letter}")
        autogenenerated_confusables += 1
    except KeyError:
        continue # there's no Q or X
    CONFUSABLES[smallcap] = letter

logger.info(f"auto-generated {autogenenerated_confusables} confusables")

# 'I'->'l' and 'm'->'rn' are in there; whitelist the latin alphabet
dropped_confusables = 0
for c in itertools.chain(ascii_letters):
    try:
        CONFUSABLES.pop(c)
    except KeyError:
        continue
    dropped_confusables += 1

logger.info(f"dropped {dropped_confusables} confusables")
logger.info(f"done, loaded {len(CONFUSABLES)} confusables")

CONFUSABLES_TRANSLATION = str.maketrans(CONFUSABLES)

# teeny tiny cache to negligibly optimize ListenerCog.on_message_edit
@functools.lru_cache(maxsize=4)
def get_clean_content(s: str):
    s = change_lookalikes(s)
    s = s.lower()
    s = strip_markdown(s)
    s = strip_space_runs(s)
    s = strip_emoji(s)
    s = strip_unicode_categories(s, ["Cf", "Cc"])
    s = s.strip()

    return s


def strip_space_runs(s: str):
    """Strip runs of whitespace down to a single space."""
    return re.sub("\\s+", " ", s)


def strip_emoji(s: str):
    """Consider any emoji with the same name as the same."""
    return re.sub("<a?:(\\w{2,}):\\d+>", ":\\1:", s)


def strip_unicode_categories(s: str, cats: Iterable[str]):
    """Strip unicode characters from the given categories. slow."""
    return "".join([c for c in s if unicodedata.category(c) not in cats])


def strip_markdown(s: str):
    """Strip markdown control characters that might get rendered invisible."""
    return re.sub("[*_\\\\]", "", s)


def change_lookalikes(s: str):
    """Swap characters that look identical to a latin alphabet character
    with their lookalike from the latin alphabet."""
    return s.translate(CONFUSABLES_TRANSLATION)
