"""ROBOT9000 background tasks"""
import logging
import time

import asyncio
import discord

from discord.ext import commands
from discord.ext import tasks

from robot9000.bot import ROBOT9000
from robot9000.model import Punishment


logger = logging.getLogger(__name__)


class BackgroundTasks(commands.Cog):
    """Background tasks for ROBOT9000"""

    def __init__(self, bot: ROBOT9000):
        self.bot: ROBOT9000 = bot
        # pylint: disable=no-member
        # (yes they do, stupid)
        self.unmute_loop.start()
        self.decay_loop.start()
        self.watchdog.start()
        self._retry_after: dict[str, int] = {}

    @tasks.loop(seconds=1)
    async def unmute_loop(self):
        c = self.bot.punishments()
        now = int(time.time())
        # backwards? ????????
        # it works and i'm not touching it
        async for doc in c.find({"end": {"$lt": now}}):
            asyncio.create_task(self.unmute(doc))

    async def unmute(self, pun: Punishment):
        now = int(time.time())
        retry_after: int | None = self._retry_after.get(pun["_id"])
        if (retry_after is not None) and (retry_after > now):
            return

        logger.info(f"unmuting {pun['user_str']} after {pun['length']}s")
        try:
            await self.bot._connection.http.remove_role(
                pun["guild_id"],
                pun["user_id"],
                pun["role_id"],
            )
        except discord.errors.Forbidden:
            retry_after = now + 10
            time_over = now - pun["end"]
            logger.warn(
                f"Forbidden: Failed to unmute @{pun['user_str']} in <#{pun['channel_id']}> | {retry_after=} | {time_over=}"
            )
            self._retry_after[pun["_id"]] = retry_after
            if time_over > 500:
                try:
                    chan = self.bot.get_channel(pun["channel_id"])
                    if isinstance(chan, discord.abc.Messageable):
                        await chan.send(
                            f"Failed to unmute <@{pun['user_id']}>; giving up (maybe permissions problem?)"
                        )
                    await self.bot.punishments().delete_one(pun)
                    return
                except:
                    # discord itself is probably down
                    logger.warn(f"> failed to send the warning about not being able to unmute; not failing and just retrying after 500s")
                    self._retry_after[pun["_id"]] = now + 500
                    return
        await self.bot.punishments().delete_one(pun)
        try:
            self._retry_after.pop(pun["_id"])
        except KeyError:
            pass

        # user = self.bot.get_user(pun["user_id"])  # borked
        # logger.info(f"Unmuting {user!s} after {pun['length']}s")

    @tasks.loop(hours=24)
    async def decay_loop(self):
        # iterate over channels
        async for channel in self.bot.config.find():
            collection = self.bot.channel_users(
                discord.PartialMessageable(id=channel["_id"], state=self.bot._get_state())
            )
            async for user in collection.find({"streak": {"$gt": 0}}):
                collection.update_one({"_id": user["_id"]}, {"$inc": {"streak": -1}})

    # TODO: watchdog that isn't isself a task
    @tasks.loop(hours=1)
    async def watchdog(self):
        for loop in [self.unmute_loop, self.unmute_loop]:
            if not loop.is_running():
                name = f"{self.__class__.__name__}.{loop.coro.__name__}"
                logger.error(f"{name} not running; restarting...")
                loop.start()


async def setup(bot: ROBOT9000):
    await bot.add_cog(BackgroundTasks(bot))
