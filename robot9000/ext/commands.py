"""Luser commands for ROBOT9000."""

import logging
import time
from typing import Optional
import pprint

import discord.abc
from discord import TextChannel, Member, Guild, User
from discord.ext import commands
from discord.ext.commands import Context
import pymongo.errors

import robot9000.bot
import robot9000.helpers
from robot9000.helpers import stupid_workaround
import robot9000.views
from robot9000.clean import get_clean_content


logger = logging.getLogger(__name__)

owner_ids = [
    247841704386756619,  # SIGSTKFLT
    415004648555151380,  # Hopeless_Platonic
]


def owner_or_manage_channels():
    async def pred(ctx):
        if ctx.author.id in owner_ids:
            return True
        return await commands.has_permissions(manage_channels=True).predicate(ctx)

    return commands.check(pred)


class ModCommands(robot9000.helpers.R9KCog):
    """Contains commands for configuring channels."""

    def _logger(self):
        return logging.getLogger(__name__)

    async def cog_command_error(self, ctx, error: Exception):
        if isinstance(error, commands.MissingPermissions):
            print(error.missing_permissions)
            words: list = [
                x.replace("_", " ").title() for x in error.missing_permissions
            ]
            if len(words) > 1:
                words[-1] = "and " + words[-1]
            if len(words) > 2:
                sentence = ", ".join(words)
            else:
                sentence = " ".join(words)
            await ctx.reply(f"You require {sentence} to do that.")
            return
        elif isinstance(error, (commands.UserInputError, commands.CheckFailure)):
            await ctx.reply(f"\u26a0 {error!s}")
            return
        await super().cog_command_error(ctx, error)

    @commands.command()
    @owner_or_manage_channels()
    async def setup(
        self,
        ctx: Context[robot9000.bot.ROBOT9000],
        chan: discord.TextChannel | None = None,
    ):
        chan = stupid_workaround(ctx, chan)

        # check if the channel is already set up
        try:
            await ctx.bot.get_channel_config(chan)
        except robot9000.helpers.NoSuchChannelException:
            pass
        else:
            await ctx.reply(
                "this channel is already set up! did you want `reset_config`?"
            )
            return

        # document we're going to insert
        doc = robot9000.helpers.default_config(chan)

        await ctx.bot.set_channel_config(chan, doc, upsert=True)
        await ctx.reply(f"Set ROBOT9000 up to listen to {chan.mention}")

    @commands.command()
    @owner_or_manage_channels()
    async def reset_config(
        self,
        ctx: Context[robot9000.bot.ROBOT9000],
        chan: discord.TextChannel | None = None,
    ):
        chan = stupid_workaround(ctx, chan)
        # don't do anything if
        try:
            await ctx.bot.get_channel_config(chan)
        except robot9000.helpers.NoSuchChannelException:
            await ctx.reply("this channel not set up! did you want `setup`?")
            return
        chan = stupid_workaround(ctx, chan)
        await ctx.bot.get_channel_config(chan)  # ensure it already exists
        await ctx.bot.set_channel_config(chan, robot9000.helpers.default_config(chan))

    @commands.command()
    @owner_or_manage_channels()
    async def teardown(self, ctx: Context, chan: Optional[TextChannel] = None):
        chan = stupid_workaround(ctx, chan)
        # confirm it exists; we don't actually need to know the details
        await ctx.bot.get_channel_config(chan)

        view = robot9000.views.TeardownView(ctx, chan)
        await ctx.reply(
            "Are you sure? This will delete all data I have for "
            f"{chan.mention}, including logs.",
            view=view,
        )
        await view.wait()

    @commands.command()
    @owner_or_manage_channels()
    async def config(self, ctx: Context, chan: Optional[TextChannel] = None):
        chan = stupid_workaround(ctx, chan)
        await ctx.bot.get_channel_config(chan)  # check if it exists
        view = await robot9000.views.ConfigView(ctx, chan)  # type: ignore - yes it is awaitable
        # mention the author because it's common for the invoking message to
        # get deleted
        await ctx.reply(
            f"config menu of {chan.mention}" + f" for {ctx.author.mention}", view=view
        )

    @commands.command()
    @owner_or_manage_channels()
    async def dump_config(self, ctx: Context, chan: Optional[TextChannel] = None):
        chan = stupid_workaround(ctx, chan)
        config = await ctx.bot.get_channel_config(chan)  # check if it exists
        await ctx.reply("```" + pprint.pformat(config) + "```")

    @commands.command()
    @owner_or_manage_channels()
    async def manual_add(self, ctx: Context, *, content):
        content = get_clean_content(content)
        collection = ctx.bot.channel_messages(ctx.channel)
        logger.info(f"manual_add: {content!r}")
        await collection.insert_one({"_id": content})

    @commands.command()
    @owner_or_manage_channels()
    async def set_streak(self, ctx: Context, who: Member, what: int):
        if what < 0:
            await ctx.reply("cowardly refusing to set streak to a negative value")
            return
        async with ctx.typing():
            collection = ctx.bot.channel_users(ctx.channel)
            await collection.update_one({"_id": who.id}, {"$set": {"streak": what}})
        await ctx.reply("Done.")

    @commands.group()
    async def punishments(self, ctx):
        pass

    @punishments.command(name="list")
    async def punishments_list(self, ctx: Context, chan: Optional[TextChannel] = None):
        now = int(time.time())
        chan = stupid_workaround(ctx, chan)
        collection = ctx.bot.punishments()
        lines = ""
        async for pun in collection.find({"channel_id": chan.id}):
            print(pun)
            time_remaining = pun["end"] - now
            length = pun["length"]
            # TODO: convert time_remaining and length to minutes, hours if they're long enough
            lines += (
                f"{pun['_id']} | {time_remaining:5}/{length:5}s | {pun['user_str']}\n"
            )
        if lines == "":
            await ctx.reply("No active punishments found.")
        else:
            await ctx.reply("```\n" + lines + "\n```")

    @commands.command()
    @commands.is_owner()
    async def pardon_me(self, ctx, chan: Optional[TextChannel] = None):
        # TODO
        pass


async def setup(bot: robot9000.bot.ROBOT9000):
    await bot.add_cog(ModCommands())
