"""Reacts to messages containing 'soon' with :tm:"""


import re
import time
import logging

import discord
from discord.ext import commands
from robot9000.clean import get_clean_content

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class SoonCog(commands.Cog):
    """Reacts to messages containing 'soon' with :tm:"""

    SOON_REGEX = "[Ss][oO0]{2}[nN]"

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        if re.search(self.SOON_REGEX, message.content) is not None:
            await message.add_reaction("™")


class FestiveJailCog(commands.Cog):
    FESTIVE_BANNED = [
        "Hashire sori yo",
        "Kaze no you ni",
        "Tsukimihara wo",
        "Padoru",
        "pagura",
        ":FestiveSpin:",
    ]

    FESTIVE_ALLOWED = [
        ":PadoruBonk",
    ]

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        if message.author.bot:
            return
        if time.gmtime().tm_mon != 12:
            content = get_clean_content(message.content)
            for banned in self.FESTIVE_BANNED:
                if banned.lower() in content:
                    for allowed in self.FESTIVE_ALLOWED:
                        if allowed.lower() in content:
                            return
                    await message.reply("no festive")
                    return

class BangRankCog(commands.Cog):
    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        if message.content.lower().startswith("!rank"):
            await message.reply("https://cdn.discordapp.com/attachments/1076799292838268978/1076936933852323950/TohruRank.gif")

async def setup(bot: commands.Bot):
    logger.info("Jokes enabled")
    await bot.add_cog(FestiveJailCog())
    await bot.add_cog(SoonCog())
    await bot.add_cog(BangRankCog())
