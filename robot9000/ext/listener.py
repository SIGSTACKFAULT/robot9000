"""Listen for messages, figure out if they're duplicates,
and deal with them."""

import asyncio
from enum import Enum
import hashlib
import logging
import time
from typing import List, Optional
import subprocess
import json

from async_lru import alru_cache
import discord
from discord import Message
from discord.ext import commands
import motor.motor_asyncio as motor
import pymongo.errors
import yarl
from vosk import Model, KaldiRecognizer

from robot9000.clean import get_clean_content
import robot9000.helpers
from robot9000.bot import ROBOT9000
from robot9000.model import ChannelConfig, Punishment
import robot9000.punishments
from robot9000.proxybots import find_candidate


logger = logging.getLogger(__name__)
# logger.setLevel(logging.DEBUG)

SAMPLE_RATE = 16000
model = Model(lang="en-us")


class _R9KCheckResult(Enum):
    DUPE = 0
    UNIQUE = 1
    BLANK = 2

    def __bool__(self):
        return self.value > 0


class ListenerCog(commands.Cog):
    """Listens for messages and deals with them."""

    def __init__(self, bot: ROBOT9000, max_content_len: int = 1024):
        super().__init__()
        self.max_content_len = max_content_len
        self.bot: ROBOT9000 = bot

    @commands.Cog.listener()
    async def on_message(self, msg: discord.Message):
        asyncio.create_task(self.handle_message(msg))

    @commands.Cog.listener()
    async def on_message_edit(self, old: Message, new: Message):
        if get_clean_content(old.content) == get_clean_content(new.content):
            # embed got updated or something
            return
        asyncio.create_task(self.handle_message(new))

    async def handle_message(self, msg: discord.Message):
        """Figure out if the channel is relevant and deal with the message
        if it's a dupe."""
        try:
            config: ChannelConfig
            config = await self.bot.get_channel_config(msg.channel)
        except robot9000.helpers.NoSuchChannelException:
            return  # not our problem

        if msg.webhook_id is not None:
            original = await find_candidate(msg)
            if original is None:
                return  # webhook, but not a proxy one
            await self._handle_proxied_message(msg, original, config)
            return

        if msg.author.bot:
            return

        if (
            len(msg.attachments) == 1
            and msg.attachments[0].filename == "voice-message.ogg"
        ):
            # voice message
            unique = await self.check_voice_message(msg)
        else:
            unique = await self.check_message(msg)
        if not unique:
            await self.punish_message(msg, config)
            await self.punish_author(msg.author, msg)

    def _log_message(
        self,
        unique: bool,
        author: discord.Member | discord.User,
        content: str,
        note: str | None = None,
    ):
        marker = "U" if unique else "D"
        logger.info(
            f">> [{marker}] <{author!s}>"
            + (f" ({note}) " if note else " ")
            + f"{content}"
        )

    async def _handle_proxied_message(
        self, proxied: Message, original: Message, config: ChannelConfig
    ):
        # TODO: find systems willing to test this extensively
        # SHOULD never false-positive, though.
        proxied_unique = await self.check_message(proxied)
        original_punishment: Punishment = await self.bot.punishments().find_one(
            {"message_id": original.id}
        )
        original_unique = original_punishment is None
        logger.debug(
            f"_handle_proxied_message: {proxied_unique=}, " + f"{original_unique=}"
        )

        if proxied_unique and original_unique:
            # neither is a dupe
            return
        if (not proxied_unique) and original_unique:
            # proxied message is dupe, original is unique
            if proxied.content == original.content:
                return  # autoproxy
            await self.punish_message(proxied, config)
            await self.punish_author(original.author, proxied)
            return
        if proxied_unique and (not original_unique):
            # proxied message is unique, original is dupe
            # I literally had to intentionally contrive this situation to test
            await self.undo_punishment(original_punishment)
            return
        if (not proxied_unique) and (not original_unique):
            # both are dupes. author was already punished.
            await self.punish_message(proxied, config)
            return
        raise ValueError

    async def undo_punishment(self, pun: Punishment):
        logger.warning("TODO: undo_punishment")
        # delete it
        result = await self.bot.punishments().delete_one(pun)
        if result.deleted_count == 0:
            return  # already done lol
        await self.bot._connection.http.remove_role(
            pun["guild_id"],
            pun["user_id"],
            pun["role_id"],
        )

    async def check_message(self, m: discord.Message) -> bool:
        """Check if a message is a duplicate. Return True if it's unique."""
        content_ok = await self.check_message_content(
            m.content,
            self.bot.channel_messages(m.channel),
        )
        attachments_ok = await self.check_message_attachments(m)
        unique = bool(content_ok) and bool(attachments_ok)
        self._log_message(unique, m.author, repr(m.clean_content))
        return unique

    async def check_voice_message(self, m: discord.Message) -> bool:
        """check if a voice message is unique"""
        voice_data = await m.attachments[0].read()
        with subprocess.Popen(
            [
                "ffmpeg",
                "-loglevel",
                "quiet",
                "-i",
                "-",
                "-ar",
                str(SAMPLE_RATE),
                "-ac",
                "1",
                "-f",
                "s16le",
                "-",
            ],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
        ) as process:
            (stdout, stderr) = process.communicate(voice_data)
            if stderr:
                logger.warn("ffmpeg failed:", stderr)
                return True
            rec = KaldiRecognizer(
                model, SAMPLE_RATE
            )  # Large vocabulary free form recognition
            rec.AcceptWaveform(stdout)
            result = json.loads(rec.Result())["text"]
            unique = bool(
                await self.check_message_content(
                    result,
                    self.bot.channel_messages(m.channel),
                )
            )
            self._log_message(unique, m.author, result, note="voice")
            return unique
        # return bool(await self.check_message_content("asdf"))

    async def check_message_content(
        self,
        content: str,
        collection: motor.AsyncIOMotorCollection,
    ) -> _R9KCheckResult:
        """Figure out if the content of the given message is unique.
        Return True if it's unique, False if duplicate."""
        content = get_clean_content(content)
        if content == "":
            return _R9KCheckResult.BLANK
        if len(content) > self.max_content_len:
            doc = {"_id": hashlib.sha256(content.encode()).hexdigest(), "hash": True}
        else:
            doc = {"_id": content, "hash": False}
        try:
            await collection.insert_one(doc)
            logger.debug(f"[UNIQUE] {content!r}")
            return _R9KCheckResult.UNIQUE
        except pymongo.errors.DuplicateKeyError:
            logger.debug(f"[  DUPE] {content!r}")
            return _R9KCheckResult.DUPE

    async def check_message_attachments(self, m: discord.Message) -> _R9KCheckResult:
        """Figure out if the attachments of the given message are unique.
        - BLANK if there are no attachments
        - DUPE if *any* of the attachments are DUPEs
        - UNIQUE if *all* of the attachemts are UNIQUE
        """
        attachments: List[robot9000.helpers.BaseAttachmentHelper]
        attachments = [robot9000.helpers.AttachmentHelper(a) for a in m.attachments]
        # for s in m.stickers:
        #     # bodge sticker checking
        #     attachments.append(robot9000.helpers.StickerAttachmentHelper(s))

        if m.content:
            url = yarl.URL(m.content)
            if url.is_absolute():
                attachments.append(robot9000.helpers.LinkAttachmentHelper(url))

        if len(attachments) == 0:
            return _R9KCheckResult.BLANK

        collection = self.bot.channel_attachments(m.channel)
        results = await asyncio.gather(
            *[self.check_attachment(a, collection) for a in attachments]
        )
        if _R9KCheckResult.DUPE in results:
            return _R9KCheckResult.DUPE
        else:
            return _R9KCheckResult.UNIQUE

    async def check_attachment(
        self,
        a: robot9000.helpers.BaseAttachmentHelper,
        collection: motor.AsyncIOMotorCollection,
    ) -> _R9KCheckResult:
        """Check if the given attachment is unique."""
        body = await a.read()
        if body is None:
            return _R9KCheckResult.BLANK
        sha2 = hashlib.sha256()
        sha2.update(body)
        digest = sha2.hexdigest()
        logger.debug(f"hash of <{a.filename}>: {digest}")

        try:
            # we don't actually need `filename` and `content_type` but they're
            # fun to keep around for statistics
            await collection.insert_one(
                {"_id": digest, "filename": a.filename, "content_type": a.content_type}
            )
            return _R9KCheckResult.UNIQUE
        except pymongo.errors.DuplicateKeyError:
            return _R9KCheckResult.DUPE

    async def punish_message(self, m: discord.Message, config: ChannelConfig):
        logger.debug(f">>> punish_message: {m.author!s}")
        p = config["punishments"]
        if emoji := robot9000.punishments.get_emoji(p):
            await m.add_reaction(emoji)

        if config["punishments"]["delete"]:
            asyncio.get_running_loop().call_later(
                config["punishments"]["delete_delay"], asyncio.create_task, m.delete()
            )

    async def punish_author(
        self,
        author: discord.User | discord.Member,
        message: discord.Message,
    ):
        logger.debug(f">>> punish_author: {author}")
        config = await self.bot.get_channel_config(message.channel)
        if config["punishments"]["mute_role"] is None:
            return

        users = self.bot.channel_users(message.channel)
        pun = self.bot.punishments()
        author_data = await self.bot.get_user_data(message.channel, author)

        async for doc in pun.find({"user_id": author.id}):
            logger.info(
                f"{author} already has active punishment {doc['_id']}, refusing to punish again"
            )
            return

        start = int(time.time())
        length = 2 ** author_data["streak"]
        end = start + length

        assert message.guild is not None

        punishment: Punishment = {
            "start": start,
            "length": length,
            "end": end,
            "message_id": message.id,
            "channel_id": message.channel.id,
            "guild_id": message.guild.id,
            "user_id": author.id,
            "role_id": config["punishments"]["mute_role"],
            "user_str": str(author),
        }  # type: ignore - _id added by mongodb

        try:
            await message.guild._state.http.add_role(  # give them the role
                # find_candidate returns a User, and without privleged intents
                # we can't upgrade it to a Member. so we have to do this for
                # some reason. Guilds should have this as an intended feature.
                message.guild.id,
                author.id,
                config["punishments"]["mute_role"],
            )
        except discord.errors.Forbidden:
            logger.info(
                f"Forbidden: failed to punish {message.author}; not entering punishment"
            )
            return

        logger.info(f"muting {author!s} for {length}s")
        await users.update_one(  # increment their streak
            {"_id": author.id}, {"$inc": {"streak": 1}}
        ),
        await pun.insert_one(punishment)  # record the punishment


async def setup(bot: ROBOT9000):
    await bot.add_cog(ListenerCog(bot))
    await bot.load_extension("robot9000.ext.stickerbodge")


async def teardown(bot: ROBOT9000):
    await bot.unload_extension("robot9000.ext.stickerbodge")
