"""Hopefully I can delete this file someday very soon. -M"""

import logging
import discord


logger = logging.getLogger(__name__)
old_message_init = discord.Message.__init__


async def setup(bot):  # pylint: disable=unused-argument
    logger.warning("Bodging in sticker support. Rapptz, plz fix.")

    def fixed_message_init(self, *, state, channel, data):
        old_message_init(self, state=state, channel=channel, data=data)
        self.stickers = data.get("sticker_items", [])

    discord.Message.__init__ = fixed_message_init


async def teardown(bot):  # pylint: disable=unused-argument
    discord.Message.__init__ = old_message_init
