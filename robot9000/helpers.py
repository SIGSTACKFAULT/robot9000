import abc
import asyncio
import logging
import io
import sys
import time
import traceback
import typing
from typing import Optional

import aiohttp
import discord
from discord.ext import commands
import motor.motor_asyncio as motor
import pymongo.errors
import yarl

from .model import ChannelConfig


logger = logging.getLogger(__name__)
ATTACHMENT_SIZE_LIMIT = 104857600  # 100MiB


class NoSuchChannelException(commands.UserInputError):
    """Thrown when we can't find any data for a channel."""

    def __init__(self, channel: discord.abc.Snowflake):
        self.channel: discord.abc.Snowflake = channel

    def __str__(self):
        if isinstance(self.channel, discord.abc.GuildChannel):
            return f"I have no data for {self.channel.mention}"
        else:
            return f"I have no data for {self.channel}"


def default_config(channel: discord.TextChannel) -> ChannelConfig:
    """Create the document for the channel, with default settings.
    Don't insert it or anything."""
    return {
        "_id": channel.id,
        "name": channel.name,
        "guild": channel.guild.id,
        "enabled": True,
        "created": int(time.time()),
        "punishments": {
            "delete": True,
            "delete_delay": 0,
            "reaction": None,
            "mute_role": None,
        },
    }


def get_emoji_from_punishments(punishments: dict):
    """get the reaction to pass to discordpy from a punishments doc"""
    if punishments["reaction"] is None:
        raise ValueError("reactions disabled")


class ShowFullTracebackView(discord.ui.View):
    def __init__(self, err: Exception):
        super().__init__()
        self.err = err

    @discord.ui.button(
        label="Show Full Traceback",
        style=discord.ButtonStyle.green,
    )
    async def show_button(
        self,
        interaction: discord.Interaction,
        button: discord.ui.Button,
    ):
        msg = traceback.format_exception(
            type(self.err), self.err, self.err.__traceback__
        )
        content = "\n".join(msg)
        try:
            await interaction.response.send_message(content=f"```\n{content}\n```")
            new_button_text = "Shown!"
        except discord.HTTPException:
            # too long for a message; put it in a file
            await interaction.response.defer()
            assert isinstance(interaction.message, discord.abc.Messageable)
            await interaction.message.reply(
                file=discord.File(
                    io.BytesIO(content.encode()), filename="traceback.txt"
                )
            )
            new_button_text = "Sent!"
            # I don't even *know* what kind of exception the file being too
            # long would raise, so I'm not going to even handle it.
            # new_button_text = "Too long! See console."
        # await interaction.message.edit(
        #     components=[
        #         {
        #             "type": 1,
        #             "components": [
        #                 {
        #                     "type": 2,
        #                     "label": new_button_text,
        #                     "disabled": True,
        #                     "style": button.style[1],
        #                     "custom_id": button.custom_id,
        #                 }
        #             ],
        #         }
        #     ]
        # )
        self.stop()


# https://stackoverflow.com/questions/11276037/
class R9KCogMeta(abc.ABCMeta, commands.CogMeta):
    pass


class R9KCog(commands.Cog, metaclass=R9KCogMeta):
    """Parent class for R9K cogs."""

    @property
    @abc.abstractmethod
    def _logger(self) -> logging.Logger:
        ...

    def __init__(self):
        super().__init__()

    async def cog_command_error(self, ctx: commands.Context, err: Exception):
        if isinstance(err, commands.CheckFailure):
            msg = traceback.format_exception(type(err), err, err.__traceback__)
            self._logger.warning(msg)

            view: Optional[discord.ui.View]
            if getattr(ctx.bot, "error_traceback_buttons", False):
                view = ShowFullTracebackView(err)
            else:
                view = None

            await ctx.send(
                f"⚠ {err!s}",
                view=view,
            )
        elif isinstance(err.__cause__, pymongo.errors.ConnectionFailure):
            await ctx.reply("database connection failed!")
        else:
            await ctx.bot.on_error(ctx, err)


def stupid_workaround(
    ctx: commands.Context,
    chan: discord.abc.Messageable | None,
) -> discord.TextChannel:
    # https://stackoverflow.com/questions/59807925/
    # THAT'S WHAT I WAS DOING BEFORE WRITING THIS FUNCTION
    chan2: discord.abc.Messageable = chan or ctx.channel
    if isinstance(chan2, discord.Thread):
        assert isinstance(chan2.parent, discord.TextChannel)
        return chan2.parent
    if not isinstance(chan2, discord.TextChannel):
        raise commands.errors.CheckFailure(f"must be a guild text channel")
    return chan2


# https://stackoverflow.com/a/45364670
class aobject(object):
    """Inheriting this class allows you to define an async __init__.

    So you can create objects by doing something like `await MyClass(params)`
    """

    async def __new__(cls, *a, **kw):
        instance = super().__new__(cls)
        await instance.__init__(*a, **kw) # type: ignore
        return instance


class BaseAttachmentHelper(abc.ABC):
    """Class which unifies the like three different
    kinds of attachment we have to deal with."""

    @abc.abstractmethod
    async def read(self) -> bytes | None:
        """read bytes up to ATTACHMENT_SIZE_LIMIT"""

    async def response_pred(self, response: aiohttp.ClientResponse):
        return True

    async def _read_intl(self, url):
        async with aiohttp.ClientSession() as session:
            resp: aiohttp.ClientResponse
            async with session.get(url) as resp:
                self.content_type = resp.content_type
                if await self.response_pred(resp):
                    try:
                        return await resp.content.readexactly(ATTACHMENT_SIZE_LIMIT)
                    except asyncio.IncompleteReadError as ex:
                        return ex.partial

    @property
    @abc.abstractmethod
    def filename(self):
        ...


class AttachmentHelper(BaseAttachmentHelper):
    def __init__(self, a: discord.Attachment):
        self.a: discord.Attachment = a

    async def read(self):
        return await self._read_intl(self.a.url)

    @property
    def filename(self):
        return self.a.filename


class StickerAttachmentHelper(BaseAttachmentHelper):
    def __init__(self, sticker_item: dict):
        self.data = sticker_item

    async def read(self):
        return await self._read_intl(
            f"https://media.discordapp.net/stickers/{self.data['id']}.png"
        )

    @property
    def filename(self):
        return self.data["name"]


class LinkAttachmentHelper(BaseAttachmentHelper):
    def __init__(self, url: yarl.URL):
        self.url = url

    async def read(self):
        return await self._read_intl(self.url)

    async def response_pred(self, response: aiohttp.ClientResponse):
        logger.info(f"LinkAttachment: {response.content_type=}")
        return response.content_type in [
            "image/png",
            "image/gif",
            "image/jpg",
            "image/webp",
        ]

    @property
    def filename(self):
        return self.url.name
