"""TypedDict definitions for ROBOT9000"""

import typing
from typing import TypedDict


class _ReactionDict(TypedDict):
    id: int
    name: str
    animated: bool


class PunishmentsConfig(TypedDict):
    """Describes how a message should be punished."""

    delete: bool
    delete_delay: int
    reaction: typing.Union[None, str, _ReactionDict]
    mute_role: typing.Optional[int]


class ChannelConfig(TypedDict):
    """Describes a channel which R9K is configurd to listen to."""

    _id: int
    guild: int
    name: str
    enabled: bool
    created: int
    punishments: PunishmentsConfig


class Punishment(TypedDict):
    """Describes a currently-active punishment."""

    _id: str  # added automatically by mongodb
    length: int
    start: int
    end: int
    message_id: int
    channel_id: int
    guild_id: int
    user_id: int
    role_id: int
    user_str: str
