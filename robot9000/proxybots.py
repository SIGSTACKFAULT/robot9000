"""
ROBOT9000 handling logic for PluralKit, TupperBox, NQN, and similar bots.
"""

import asyncio
import logging
import typing

import discord


logger = logging.getLogger(__name__)
# logger.setLevel(logging.DEBUG)


async def find_candidate(
    msg: discord.Message
) -> discord.Message | None:
    prev: discord.Message
    #asyncio.sleep(1)
    async for prev in msg.channel.history(limit=10):
        # search for messages whose content is a superset
        # e.g., "hello, world!" will actually punish "ju;hello, world!"
        if prev.author.bot:
            continue
        logger.debug(f">> looking at message <{prev.author!s}> {prev.content!r}")
        if msg.content in prev.content:
            return prev
    return None
