import discord

from .model import PunishmentsConfig


def reactions_possible(p: PunishmentsConfig):
    """Is it even possible to react given the setup in `p`?"""
    if (p["delete"] is True) and (p["delete_delay"] > 0):
        # delete after delay
        return True
    if (p["delete"] is True) and (p["delete_delay"] == 0):
        # delete instantly
        return False
    if p["delete"] is False:
        # don't delete
        return True
    raise ValueError("corrupted config")


def reactions_enabled(p: PunishmentsConfig):
    """Should we react?"""
    return reactions_possible(p) and p["reaction"] is not None


def get_emoji(p: PunishmentsConfig):
    """get the emoji if we're supposed to react; None otherwise"""
    if reactions_enabled(p):
        if isinstance(p["reaction"], str):
            # unicode emoji
            return p["reaction"]
        else:
            # custom emoji
            return discord.PartialEmoji(**p["reaction"])
    else:
        return None
