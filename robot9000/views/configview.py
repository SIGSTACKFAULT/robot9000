import asyncio
import logging

import discord
from discord.ext import commands
import motor.motor_asyncio as motor

import robot9000.bot
from .. import helpers
from .. import punishments


logger = logging.getLogger(__name__)


class ConfigDeleteSelect(discord.ui.Select):
    def __init__(
        self,
        ctx: commands.Context[robot9000.bot.ROBOT9000],
        channel: discord.TextChannel,
        delete_state: bool,
    ):
        self.ctx = ctx
        self.channel = channel
        super().__init__(
            placeholder=f"Delete Dupes: {'ON' if delete_state else 'OFF'}",
            options=[
                discord.SelectOption(label="ON", value="1"),
                discord.SelectOption(label="OFF", value="0"),
            ],
            row=0,
        )

    async def callback(
        self,
        interaction: discord.Interaction[robot9000.bot.ROBOT9000],
    ):
        assert interaction.data is not None
        values = interaction.data.get("values")
        assert values is not None
        new_state = values[0] == "1"
        await self.ctx.bot.update_channel_config(
            self.channel,
            {"$set": {"punishments.delete": new_state}},
        )
        await interaction.response.defer()


class ConfigRoleSelect(discord.ui.RoleSelect):
    def __init__(
        self,
        ctx: commands.Context[robot9000.bot.ROBOT9000],
        channel: discord.TextChannel,
        mute_role: discord.Role | None,
    ):
        self.ctx = ctx
        self.channel = channel
        super().__init__(
            placeholder=f"Mute Role: {mute_role}",
            min_values=0,
            row=1,
        )

    async def callback(
        self,
        interaction: discord.Interaction[robot9000.bot.ROBOT9000],
    ):
        assert interaction.data is not None
        values = interaction.data.get("values")
        assert values is not None
        if len(values) == 0:
            await self.ctx.bot.update_channel_config(
                self.channel,
                {"$set": {"punishments.mute_role": None}},
            )
        else:
            role_id = values[0]
            await self.ctx.bot.update_channel_config(
                self.channel,
                {"$set": {"punishments.mute_role": role_id}},
            )
        await interaction.response.defer()


class ConfigReactionButton(discord.ui.Button):
    def __init__(
        self,
        ctx: commands.Context[robot9000.bot.ROBOT9000],
        channel: discord.TextChannel,
        punishments_config: punishments.PunishmentsConfig,
    ):
        self.view: ConfigView
        self.ctx = ctx
        self.channel = channel
        # self.config_emoji = emoji
        super().__init__(
            label=(
                "Set Reaction"
                if punishments_config["reaction"] == None
                else "Change Reaction"
            ),
            row=2,
            emoji=punishments.get_emoji(punishments_config),
        )

    async def callback(
        self,
        interaction: discord.Interaction[robot9000.bot.ROBOT9000],
    ):
        assert interaction.message is not None
        react_to_me = await interaction.message.reply(
            "react with the emoji you want to use (times out in 60s)"
        )
        assert react_to_me is not None
        await interaction.response.defer()

        # self.ctx.bot.wait_for("reaction_add")
        def check(reaction: discord.Reaction, user: discord.User):
            return reaction.message == react_to_me and user == interaction.user

        while True:
            try:
                reaction: discord.Reaction
                reaction, _ = await self.ctx.bot.wait_for(
                    "reaction_add", timeout=60, check=check
                )
            except asyncio.TimeoutError:
                await react_to_me.edit(content=f"(timed out)", delete_after=3)
                return
            if isinstance(reaction.emoji, discord.PartialEmoji):
                # we can't use it because we're not in the server which has it
                await react_to_me.edit(
                    content=f"I can't use {reaction.emoji} because i'm not in the server it's in :(\ntry again?\n(60s timeout reset)",
                )
                try:
                    await reaction.clear()
                except:
                    pass  # we probably don't have the permission, that's fine
                continue
            break
        if isinstance(reaction.emoji, str):
            # unicode emoji
            doc = reaction.emoji
        else:
            # custom emoji
            doc = {
                "id": reaction.emoji.id,
                "name": reaction.emoji.name,
                "animated": reaction.emoji.animated,
            }
        # await react_to_me.reply("```" + pprint.pformat(doc) + "```")
        await react_to_me.edit(content=f"reaction set to {reaction.emoji}")
        await self.ctx.bot.update_channel_config(
            self.channel,
            {"$set": {"punishments.reaction": doc}},
        )
        await self.view.update(interaction)


class ConfigReactionClearButton(discord.ui.Button):
    def __init__(
        self,
        ctx: commands.Context[robot9000.bot.ROBOT9000],
        channel: discord.TextChannel,
        punishments_config: punishments.PunishmentsConfig,
    ):
        self.view: ConfigView
        self.ctx = ctx
        self.channel = channel
        # self.config_emoji = emoji
        super().__init__(
            label="Clear", row=2, disabled=(punishments_config["reaction"] == None)
        )

    async def callback(self, interaction: discord.Interaction):
        await self.ctx.bot.update_channel_config(
            self.channel,
            {"$set": {"punishments.reaction": None}},
        )
        await interaction.response.defer()
        await self.view.update(interaction)


class ConfigDelayIncrementButton(discord.ui.Button):
    def __init__(
        self,
        ctx: commands.Context[robot9000.bot.ROBOT9000],
        channel: discord.TextChannel,
        punishments_config: punishments.PunishmentsConfig,
    ):
        self.view: ConfigView
        self.ctx = ctx
        self.channel = channel
        super().__init__(
            label="+1",
            row=3,
            # disabled=punishments_config["delete_delay"] < 60,
        )

    async def callback(self, interaction: discord.Interaction):
        await self.ctx.bot.update_channel_config(
            self.channel,
            {"$inc": {"punishments.delete_delay": 1}},
        )
        await self.view.update(interaction)
        await interaction.response.defer()


class ConfigDelayDisplayButton(discord.ui.Button):
    def __init__(
        self,
        ctx: commands.Context[robot9000.bot.ROBOT9000],
        channel: discord.TextChannel,
        punishments_config: punishments.PunishmentsConfig,
    ):
        self.view: ConfigView
        self.ctx = ctx
        self.channel = channel
        super().__init__(
            label=f"Delete delay: {punishments_config['delete_delay']}s",
            row=3,
            disabled=True,
        )


class ConfigDelayDecrementButton(discord.ui.Button):
    def __init__(
        self,
        ctx: commands.Context[robot9000.bot.ROBOT9000],
        channel: discord.TextChannel,
        punishments_config: punishments.PunishmentsConfig,
    ):
        self.view: ConfigView
        self.ctx = ctx
        self.channel = channel
        super().__init__(
            label="-1", row=3, disabled=punishments_config["delete_delay"] == 0
        )

    async def callback(self, interaction: discord.Interaction):
        await self.ctx.bot.update_channel_config(
            self.channel,
            {"$inc": {"punishments.delete_delay": -1}},
        )
        await self.view.update(interaction)
        await interaction.response.defer()


class ConfigView(discord.ui.View, helpers.aobject):
    """View for `config` command."""

    # yes, mypy, it can be async. I *made* it possible.
    async def __init__(  # type: ignore[misc]
        self,
        ctx: commands.Context[robot9000.bot.ROBOT9000],
        channel: discord.TextChannel,
    ):
        super().__init__(timeout=1800)
        self.ctx: commands.Context[robot9000.bot.ROBOT9000] = ctx
        self.channel = channel
        await self.update()

    async def update(self, interaction=None):
        if interaction:
            self.clear_items()
        self.config = await self.ctx.bot.get_channel_config(self.channel)
        mute_role = self.config["punishments"]["mute_role"]
        if mute_role is not None:
            assert self.ctx.channel.guild is not None
            mute_role = self.ctx.channel.guild.get_role(int(mute_role))
        self.add_item(
            ConfigDeleteSelect(
                self.ctx, self.channel, self.config["punishments"]["delete"]
            )
        )
        self.add_item(ConfigRoleSelect(self.ctx, self.channel, mute_role))
        self.add_item(
            ConfigReactionButton(self.ctx, self.channel, self.config["punishments"])
        )
        self.add_item(
            ConfigReactionClearButton(
                self.ctx, self.channel, self.config["punishments"]
            )
        )
        self.add_item(
            ConfigDelayDecrementButton(
                self.ctx, self.channel, self.config["punishments"]
            )
        )
        self.add_item(
            ConfigDelayDisplayButton(self.ctx, self.channel, self.config["punishments"])
        )
        self.add_item(
            ConfigDelayIncrementButton(
                self.ctx, self.channel, self.config["punishments"]
            )
        )
        if interaction:
            # discord will be oblivious to the fact these are different objects
            await interaction.message.edit(view=self)

    async def interaction_check(self, interaction: discord.Interaction):
        return interaction.user == self.ctx.author
