import abc

import discord
from discord.ext import commands

import robot9000.bot


class TeardownView(discord.ui.View):
    def __init__(
        self,
        ctx: commands.Context[robot9000.bot.ROBOT9000],
        channel: discord.TextChannel,
    ):
        super().__init__()
        self.ctx = ctx
        self.channel = channel

    @discord.ui.button(
        label="DO IT!",
        style=discord.ButtonStyle.red,
    )
    async def confirm_button(
        self,
        interaction: discord.Interaction,
        button: discord.ui.Button,
    ):
        await self.ctx.bot.teardown_channel(self.channel)
        await interaction.response.edit_message(
            content=f"Deleted all data for {self.channel.mention}",
            view=None,
        )

    @discord.ui.button(
        label="Cancel",
        style=discord.ButtonStyle.grey,
    )
    async def cancel_button(
        self,
        interaction: discord.Interaction,
        button: discord.ui.Button,
    ):
        await interaction.response.edit_message(
            content="Teardown cancelled",
            view=None,
        )
        self.stop()
