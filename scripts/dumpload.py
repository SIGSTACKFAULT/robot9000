"""Dump and load message databases using a really dumb custom format"""
import sys
import click
import pymongo
import pymongo.errors

# TODO: specify client arguments
client = pymongo.MongoClient()


@click.group()
def main():
    pass


@main.command()
@click.argument("channel", type=int)
@click.argument("f", type=click.File("r"), required=False,
                metavar="[FILE]", default=sys.stdin)
@click.option("--db", type=str, metavar="DATABASE")
# TODO: data other than messages
def load(db, f, channel):
    database = client[db]
    collection = database[f"channels.{channel}.messages"]

    # blah blah "no known parent package"
    # stupid problem, stupid solution
    import importlib
    import os
    location = os.path.dirname(__file__) + "/../robot9000/clean.py"
    spec = importlib.util.spec_from_file_location("clean", location)
    clean_msg = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(clean_msg)
    # print("load", collection, f, channel, file=sys.stderr)
    for line in f:
        cleaned = clean_msg.get_clean_content(line)
        try:
            collection.insert_one({"_id": cleaned})
        except pymongo.errors.DuplicateKeyError:
            pass


@main.command()
@click.argument("channel", type=int)
@click.argument("f", type=click.File("w"), required=False,
                metavar="[FILE]", default=sys.stdout)
@click.option("--db", type=str, metavar="DATABASE")
# TODO: data other than messages
def dump(db, f, channel):
    database = client[db]
    collection = database[f"channels.{channel}.messages"]
    # print("dump", collection, f, channel, file=sys.stderr)
    for obj in collection.find():
        print(obj["_id"], file=f)


if __name__ == "__main__":
    main()
