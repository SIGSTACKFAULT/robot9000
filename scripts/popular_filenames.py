from typing import Dict

import pymongo
from pymongo.collection import Collection
import click

client = pymongo.MongoClient()


@click.command()
@click.argument("database", default="r9k")
@click.argument("channel", type=int, default=None,
                required=False, metavar="CHANNEL_ID")
@click.option("--min-count", default=2,
              help="minimum appearances to bother displaying")
@click.option("--extensions/--filenames", default=False,
              help="measure full filenames or just extensions?")
def main(database: str, channel: int,
         min_count: int = 2, extensions: bool = False):
    """Looks in the database and finds the most popular filenames.
    Will probably work with no arguments."""
    db = client[database]

    channels = []
    if channel is None:
        for chan in db.list_collections():
            if chan["name"].endswith("attachments"):
                channels.append(db[chan["name"]])
    else:
        channels = [db[f"channels.{channel}.attachments"]]

    results: Dict = {}
    for chan in channels:
        for a in chan.find():
            filename = a['filename']
            if extensions:
                filename = filename.split('.')[-1]
            results[filename] = results.get(filename, 0) + 1

    results_list = list(results.items())
    results_list = sorted(results_list, key=lambda x: x[1], reverse=True)

    width = 1
    for f, n in results_list:
        width = max(width, len(str(n)))

    for f, n in results_list:
        print(f"{n:>{width}} | {f}")


main()
