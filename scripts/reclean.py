#!/usr/bin/env python3

import asyncio
import importlib.util
import itertools
import os
import time

import motor.motor_asyncio as motor
import click


try:
    import robot9000.clean as clean
except ImportError:
    location = os.path.dirname(__file__) + "/../robot9000/clean.py"
    spec = importlib.util.spec_from_file_location("clean", location)
    clean = importlib.util.module_from_spec(spec)  # type: ignore
    spec.loader.exec_module(clean)  # type: ignore


@click.command()
@click.argument("channel", type=int)
@click.option("--db", type=str, default="r9k", metavar="DATABASE",
              help="Database to use; default is `r9k`.")
@click.option("-q", "--quiet", is_flag=True,
              help="Shut up about how fast I am.")
def main(*args, **kwargs):
    """Run `robot9000.clean.get_clean_content()` on all messages in CHANNEL."""
    start = time.time()
    count = asyncio.run(amain(*args, **kwargs))
    end = time.time()
    if not kwargs["quiet"]:
        delta = end - start
        print(f"recleaned {count} messages in {delta:.2f}s")


async def amain(channel: int, db: str, quiet: bool):
    # TODO: client options on command-line
    client = motor.AsyncIOMotorClient()
    database = client[db]
    messages = database[f"channels.{channel}.messages"]
    count = 0
    tasks = []
    # start all the replacements
    async for msg in messages.find():
        recleaned = clean.get_clean_content(msg["_id"])
        count += 1
        tasks.append(
            messages.replace_one(msg, {"_id": recleaned})
        )
    # wait for them all to finish
    for task in tasks:
        await task
    return count


main()
